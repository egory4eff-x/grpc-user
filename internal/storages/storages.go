package storages

import (
	"gitlab.com/egory4eff-x/grpc-user/internal/db/adapter"
	"gitlab.com/egory4eff-x/grpc-user/internal/infrastructure/cache"
	ustorage "gitlab.com/egory4eff-x/grpc-user/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
