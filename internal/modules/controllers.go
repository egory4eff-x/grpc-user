package modules

import (
	"gitlab.com/egory4eff-x/grpc-user/internal/infrastructure/component"
	ucontroller "gitlab.com/egory4eff-x/grpc-user/internal/modules/user/controller"
	"gitlab.com/egory4eff-x/grpc-user/internal/modules/user/service"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(users service.Userer, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		User: userController,
	}
}
